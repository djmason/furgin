local FrameBackdrop = {
    bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
    edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border",
    tile = true,
    tileSize = 32,
    edgeSize = 32,
    insets = { left = 8, right = 8, top = 8, bottom = 8 }
}

local function _CreateTitle(frame, name)
    local titlebg = frame:CreateTexture(nil, "OVERLAY")
    titlebg:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
    titlebg:SetTexCoord(0.31, 0.67, 0, 0.63)
    titlebg:SetPoint("TOP", 0, 12)
    titlebg:SetWidth(100)
    titlebg:SetHeight(40)

    local title = CreateFrame("Frame", "$parentTitle", frame)
    title:EnableMouse(true)
    title:SetAllPoints(titlebg)

    local titletext = title:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    titletext:SetPoint("TOP", titlebg, "TOP", 0, -14)
    titletext:SetText(name)
    titlebg:SetWidth((titletext:GetWidth() or 0) + 10)

    local titlebg_l = frame:CreateTexture(nil, "OVERLAY")
    titlebg_l:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
    titlebg_l:SetTexCoord(0.21, 0.31, 0, 0.63)
    titlebg_l:SetPoint("RIGHT", titlebg, "LEFT")
    titlebg_l:SetWidth(30)
    titlebg_l:SetHeight(40)

    local titlebg_r = frame:CreateTexture(nil, "OVERLAY")
    titlebg_r:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
    titlebg_r:SetTexCoord(0.67, 0.77, 0, 0.63)
    titlebg_r:SetPoint("LEFT", titlebg, "RIGHT")
    titlebg_r:SetWidth(30)
    titlebg_r:SetHeight(40)
end

local function _CreateFrame(width, height, name, fn)

    local frame = CreateFrame("Frame", name, UIParent)
    frame:Hide()

    frame:EnableMouse(true)
    frame:SetMovable(true)
    frame:SetResizable(false)
    frame:SetFrameStrata("FULLSCREEN_DIALOG")
    frame:SetBackdrop(FrameBackdrop)
    frame:SetBackdropColor(0, 0, 0, 1)
    frame:SetMinResize(400, 200)
    frame:SetToplevel(true)
    frame:SetSize(width, height)
    frame:ClearAllPoints()
    frame:SetPoint("CENTER")

    local closebutton = CreateFrame("Button", "$parentCloseButton", frame, "UIPanelButtonTemplate")
    closebutton:SetScript("OnClick", function(button) frame:Hide() end)
    closebutton:SetPoint("BOTTOMRIGHT", -27, 17)
    closebutton:SetHeight(20)
    closebutton:SetWidth(100)
    closebutton:SetText("Close")

    if name ~= nil then _CreateTitle(frame, name) end
    local container = CreateFrame("Frame", "$parentContainerFrame", frame)
    container:EnableMouse(true)
    container:SetPoint("TOPLEFT", 16, -20)
    container:SetPoint("BOTTOMRIGHT", -16, 48)
    if fn ~= nil then fn(container) end
    return frame
end

Furgin:RegisterMethod("CreateFrame", _CreateFrame)

