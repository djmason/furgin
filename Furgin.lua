local colourList = {
    ["HUNTER"] = { r = 0.67, g = 0.83, b = 0.45, colorStr = "ffabd473" },
    ["WARLOCK"] = { r = 0.58, g = 0.51, b = 0.79, colorStr = "ff9482c9" },
    ["PRIEST"] = { r = 1.0, g = 1.0, b = 1.0, colorStr = "ffffffff" },
    ["PALADIN"] = { r = 0.96, g = 0.55, b = 0.73, colorStr = "fff58cba" },
    ["MAGE"] = { r = 0.41, g = 0.8, b = 0.94, colorStr = "ff69ccf0" },
    ["ROGUE"] = { r = 1.0, g = 0.96, b = 0.41, colorStr = "fffff569" },
    ["DRUID"] = { r = 1.0, g = 0.49, b = 0.04, colorStr = "ffff7d0a" },
    ["SHAMAN"] = { r = 0.0, g = 0.44, b = 0.87, colorStr = "ff0070de" },
    ["WARRIOR"] = { r = 0.78, g = 0.61, b = 0.43, colorStr = "ffc79c6e" },
    ["DEATHKNIGHT"] = { r = 0.77, g = 0.12, b = 0.23, colorStr = "ffc41f3b" },
    ["MONK"] = { r = 0.0, g = 1.00, b = 0.59, colorStr = "ff00ff96" },
    ["highlight1"] = { r = 0.0, g = 0.859, b = 1.0, colorStr = "ff00dbff" },
    ["highlight2"] = { r = 0.0, g = 0.859, b = 1.0, colorStr = "ffF8DF17" },
    ["highlight3"] = { colorStr = "ffF83417" },
    ["red"] = { colorStr = "ffff0000" },
    ["green"] = { colorStr = "ff00ff00" },
    ["blue"] = { colorStr = "ff0000ff" },
    ["white"] = { colorStr = "ffffffff" },
};

local realmName = GetRealmName()
local VERSION = "0.1"
local NAME = "Furgin"
local timerEvents = {}
local commands = {}
local methods = {}

local function _colours(msg)
    local str = msg
    for name, colour in pairs(colourList) do
        local colourString = "|c" .. name
        str = string.gsub(str, colourString, "|c" .. colour.colorStr)
    end
    return str
end

local function _log(msg)
    if msg ~= nil and type(msg) ~= "table" then
        print(_colours("|cWARRIOR" .. NAME .. "|r: " .. msg));
    end
end

local function _error(msg)
    if msg ~= nil then
        print(_colours("|cWARRIOR" .. NAME .. "|r: |cred" .. msg));
    end
end

local function onUpdate(self, elapsed)
    local currentTime = time()
    for i, t in ipairs(timerEvents) do
        if t.time < currentTime then
            t.callback()
            table.remove(timerEvents, i)
        end
    end
end

SLASH_FURGIN1 = '/furgin';
function SlashCmdList.FURGIN(msg, editbox, ...)
    for c, fn in pairs(commands) do
        -- this is pretty poor, i should have some sort
        -- of matcher logic
        if string.starts(c, msg) then
            fn(msg)
        end
    end
end

function string.starts(Start, String)
    return string.sub(String, 1, string.len(Start)) == Start
end

local frame, events = CreateFrame("Frame"), {};

local lookupMethods = {
    __index = function(self, key)
        return function(self, arg1, ...)
            if not methods[key] then
                _error("method " .. key .. " missing.")
                return
            end
            return methods[key].func(arg1, ...)
        end
    end
}

function _RegisterMethod(name, method)
    _log("RegisterMethod |cgreen" .. name)
    methods[name] = { func = method }
end

function _RegisterEvent(name, method)
    _log("RegisterEvent |cgreen" .. name)
    if events[name] == nil then
        frame:RegisterEvent(name)
        events[name] = {}
    end
    table.insert(events[name], method)
end

function _OnLoad(fn)
    _RegisterEvent("ADDON_LOADED", function(self, name)
        if name == "furgin" then
            fn(self,name)
        end
    end)
end

function _RegisterTimer(time, callback)
    _log("Create Timer in |cgreen" .. time)
    table.insert(timerEvents, { time = time, callback = callback })
end

function _RegisterCommand(name, method)
    _log("RegisterCommand |cgreen" .. name)
    commands[name] = method
end

function _OnTooltipSetItem(fn)
    for _, obj in next, {
        GameTooltip,
        ShoppingTooltip1,
        ShoppingTooltip2,
        ShoppingTooltip3,
        ItemRefTooltip,
    } do
        obj:HookScript("OnTooltipSetItem", fn)
    end
end

_RegisterEvent("PLAYER_ENTERING_WORLD", function(addon)
    _RegisterTimer(time() + 2, function()
        _log(VERSION .. " Enabled")
    end)
end)

_RegisterCommand("hello", function(line)
    _log("Hello!")
end)

_RegisterMethod("RegisterMethod", _RegisterMethod)
_RegisterMethod("Colours", _colours)
_RegisterMethod("Log", _log)
_RegisterMethod("error", _error)
_RegisterMethod("RegisterEvent", _RegisterEvent)
_RegisterMethod("RegisterCommand", _RegisterCommand)
_RegisterMethod("RegisterTimer", _RegisterTimer)
_RegisterMethod("OnTooltipSetItem", _OnTooltipSetItem)
_RegisterMethod("OnLoad", _OnLoad)
setmetatable(Furgin, lookupMethods)

frame:SetScript("OnUpdate", onUpdate)
frame:SetScript("OnEvent", function(self, event, ...)
    for _, fn in ipairs(events[event]) do
        fn(self, ...)
    end
end);
