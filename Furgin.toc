## Interface: 60000
## Title: Furgin
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: GratsDataStore
## SavedVariablesPerCharacter: FurginDataStore
## Dependencies: DataStore

Furgin_Data.lua
Furgin.lua
Furgin_Sets.lua
Furgin_SetsWowHead.lua
Furgin_Frame.lua
Furgin_Grats.lua
Furgin_Damage.lua
Furgin_Debug.lua