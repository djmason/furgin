FurginDataStore = {}

local record = {}
local count = {}

local function nice(val)
    -- valid for all val 1->10
    local m = val % 10
    if m == 1 then
        return val .. "st"
    elseif m == 2 then
        return val .. "nd"
    elseif m == 3 then
        return val .. "rd"
    else
        return val .. "th"
    end
end

function string.starts(Start, String)
    return string.sub(String, 1, string.len(Start)) == Start
end

local frame, events = CreateFrame("Frame"), {};

Furgin:OnLoad(function(self,name)
    record = FurginDataStore["record"]
    count = FurginDataStore["count"]
    if record == nil then
        record = {}
    end
    if count == nil then
        count = {}
    end
    FurginDataStore = { record = record, count = count }
end)

Furgin:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED", function(timestamp, event, ...)
    local unknown, sourceGUID, sourceName, sourceFlags, sourceFlags2,
    destGUID, destName, destFlags, destFlags2 = select(1, ...)

    local playerName = GetUnitName("player");

    if destName == nill or sourceName ~= playerName or destName == playerName then
        return
    end

    local eventPrefix, eventSuffix = event:match("^(.-)_?([^_]*)$");

    if eventSuffix == "DAMAGE" then
        local name = "Unknown";
        local index = 10;

        if eventPrefix == "SWING" then
            name = "Swing";
        else
            local spellId, spellName, spellSchool = select(index, ...);
            index = index + 3;
            name = spellName;
        end

        local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = select(index, ...);

        if record[name] == nil then
            record[name] = {}
        end
        if count[name] == nil then
            count[name] = 0
        end
        local array = record[name]
        local added = false
        local index = 0
        for i = 1, 10, 1 do
            if array[i] == nil or array[i] < amount then
                table.insert(array, i, amount)
                added = true
                index = i
                break
            end
        end
        while table.getn(array) > 10 do
            table.remove(array)
        end
        count[name] = count[name] + 1
        if added then
            Furgin:Log("|cFFC79C6EAbility|r " .. name .. "|cFFC79C6E hit for |r" .. amount .. " (" .. nice(index) .. ") (" .. count[name] .. ").");
        end
    end
end)

Furgin:RegisterCommand("top", function(line)
    if line == 'top' then
        for name, data in pairs(record) do
            Furgin:Log("name: " .. name);
        end
    else
        local spell = string.sub(line, 5); -- " top "
        Furgin:Log("|cFFC79C6ETop Damage for: |r" .. spell .. "|cFFC79C6E.");
        local array = record[spell]
        if array ~= nill then
            for i, dmg in ipairs(array) do
                Furgin:Log("|cFFC79C6E(|r" .. i .. "|cFFC79C6E) hit for |r" .. dmg);
            end
        end
    end
end)

