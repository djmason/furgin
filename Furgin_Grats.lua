GratsDataStore = {}

local realmName = GetRealmName()
local achievements = {}
local highscores = {}
local gratscount = {}

local function findGuildMember(playerName)
    if playerName == nil then
        return nil
    end
    local numTotalMembers, numOnlineMaxLevelMembers, numOnlineMembers = GetNumGuildMembers();
    for i = 1, numTotalMembers, 1 do
        local name, rank, rankIndex, level, class, zone, note,
        officernote, online, status, classFileName,
        achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
        if name == playerName or name == playerName .. "-" .. realmName then
            return i;
        end
    end
    return nil;
end

local function getShortName(fullname)
    return gsub(fullname, "%-[^|]+", "")
end

local function getPlayerName(fullname)
    local index = findGuildMember(fullname)
    local name = getShortName(fullname)
    if index == nil then
        return name
    else
        local guildPlayerName, rank, rankIndex, level, class, zone, note,
        officernote, online, status, classFileName,
        achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(index);
        return "|c" .. classFileName .. name .. "|r"
    end
end

local function playerField(player, field, value)
    Furgin:Log("|cFFFFF569" .. field .. "|r for " .. getPlayerName(player) .. ": " .. value)
end

local function grat(player, count)
    PlaySound("LEVELUPSOUND")

    if gratscount[player] == nil then
        gratscount[player] = count
    else
        gratscount[player] = gratscount[player] + count
    end

    if highscores[player] == nil then
        highscores[player] = 0;
    end
    if highscores[player] < count then
        highscores[player] = count;
        playerField(player, "High score", count)
    end

    local message = "";
    local name = " " .. getShortName(player); --" "..player; --GetUnitName(player);
    local countString = "(" .. gratscount[player] .. ")"
    local testName = getPlayerName(player);
    if count == 1 then
        message = "Grats" .. name .. "!"
    elseif count == 2 then
        message = "Double Grats" .. name .. "!"
    elseif count == 3 then
        message = "Triple Grats" .. name .. "!"
    else
        message = count .. "xGrats" .. name .. "!"
    end
    if GetUnitName("player") == "Furgin" then
        SendChatMessage(message, "GUILD")
    else
        Furgin:Log("Current Player: " .. GetUnitName("player"))
    end
end

local function _GuildAchievement(sender)
    local t = time();
    local sendMessage = true;
    local delay = 1 + math.random(2);
    local count = (achievements[sender] ~= nil and (achievements[sender]+1) or 1)
    achievements[sender] = count;
    Furgin:RegisterTimer(time()+delay,
        function()
            if achievements[sender] == count then
                grat(sender,count)
                achievements[sender] = nil
            end
        end)
    playerField(sender, "Achievement count", achievements[sender])
end

Furgin:RegisterEvent("CHAT_MSG_GUILD_ACHIEVEMENT", function(self,...)
    local message, sender = ...;
    _GuildAchievement(sender)
end)

Furgin:OnLoad(function(self,name)
    highscores = GratsDataStore["highscores"]
    gratscount = GratsDataStore["gratscount"]
    if highscores == nil then
        highscores = {}
    end
    if gratscount == nil then
        gratscount = {}
    end
    GratsDataStore = { gratscount = gratscount, highscores = highscores }
end)

Furgin:RegisterCommand("grats", function()
    Furgin:Log(GetUnitName("player"))
    Furgin:Log("Grats Count:")
    Furgin:Log("------------")
    for player, grats in pairs(gratscount) do
        Furgin:Log(getPlayerName(player) .. ": " .. grats);
    end

    Furgin:Log("")
    Furgin:Log("HighScores:")
    Furgin:Log("-----------")
    for player, score in pairs(highscores) do
        Furgin:Log(getPlayerName(player) .. ": " .. score);
    end
    Furgin:Log("")
end)

--Furgin:RegisterCommand("grats test", function()
--    _GuildAchievement("Furgin")
--    _GuildAchievement("Furgin")
--    _GuildAchievement("Furgin")
--end)