local function _AddItemDebugInfo(tooltip)
    local itemName, itemLink = tooltip:GetItem()
    if itemLink ~= nil then
        local itemString = string.match(itemLink, "item[%-?%d:]+")
        local _, itemId, enchantId, jewelId1, jewelId2, jewelId3,
        jewelId4, suffixId, uniqueId, linkLevel, reforgeId = strsplit(":", itemString)

        local line1 = "ItemID: |cFFFFFFFF" .. itemId
        local itemName, itemLink, itemRarity, itemLevel, itemMinLevel, itemType, itemSubType,
        itemStackCount, itemEquipLoc, itemTexture, itemSellPrice = GetItemInfo(itemId)

        tooltip:AddLine(" ")
        tooltip:AddLine(line1)

        if itemLevel ~= nil then
            local line2 = "ItemLevel: |cFFFFFFFF" .. itemLevel
            local line3 = "ItemType: |cFFFFFFFF" .. itemType
            local line4 = "ItemSubType: |cFFFFFFFF" .. itemSubType
            tooltip:AddLine(line2)
            tooltip:AddLine(line3)
            tooltip:AddLine(line4)
        end

        tooltip:AddLine(" ")
        tooltip:Show();
    end
end

Furgin:OnTooltipSetItem(_AddItemDebugInfo)
